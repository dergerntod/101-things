using UnityEngine;
using System.Collections;

public class MicrophoneInput : MonoBehaviour {

    public float volume;
    private AudioClip record;
    public GameObject indicator;
    private Vector3 scale;
    private float timeSinceLastUpdate = 0;
    // Use this for initialization
    void Start()
    {
        volume = 0.0f;
        Debug.Log("Num Mic Devices: " + Microphone.devices.Length);
        foreach (string f in Microphone.devices)
        {
            Debug.Log(f);
        }
        scale = indicator.transform.localScale;
        audio.clip = Microphone.Start(Microphone.devices[0], true, 999, 44100);
        while (!(Microphone.GetPosition(Microphone.devices[0]) > 0))
        {

        } audio.Play();
    
    }
 
 // Update is called once per frame
    void Update()
    {
        timeSinceLastUpdate += Time.deltaTime;

            float[] data = new float[735];
            float comma = Time.realtimeSinceStartup - (int)(Mathf.Floor(Time.realtimeSinceStartup));
            audio.GetOutputData(data, 0);
            //record.GetData(data, (int)(comma * 44100));
            float sum = 0;
            foreach (float f in data)
            {
                sum += Mathf.Abs(f);
            }
            sum /= 735f;
            //Debug.Log(i);
            indicator.transform.localScale = scale + Vector3.right * sum * 100;
            volume = sum;
            Debug.Log(volume);
    }
}