using UnityEngine;
using System.Collections;

public class controller : MonoBehaviour {

    private const int power = 20;

    private Rigidbody[] rigids;
    private GameObject player;
    private MicrophoneInput mic;
    private float cooldown;

	// Use this for initialization
	void Start () {
	    rigids = (Rigidbody[])GameObject.FindObjectsOfType(typeof(Rigidbody));
        player = GameObject.FindGameObjectWithTag("Player");
        mic = (MicrophoneInput)GameObject.FindObjectOfType(typeof(MicrophoneInput));
        cooldown = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        if (cooldown > 0.0f) { cooldown -= Time.deltaTime; }
        Debug.Log(cooldown);

        if (cooldown < 0.01f && Mathf.Abs(mic.volume) > 0.01f)
        {
            cooldown = 1.0f;
            foreach (Rigidbody rigid in rigids)
            {
                Vector3 direction = rigid.transform.position - player.transform.position;
                if (direction.magnitude < power)
                {
                    rigid.AddForce(direction.normalized * (power - direction.magnitude) * 10 * Mathf.Pow(Mathf.Abs(mic.volume), 2), ForceMode.Impulse);
                }
            }
        }
	}
}
